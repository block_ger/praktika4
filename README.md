# Praktikum 4

Dieses Projekt zur weiteren Bearbeitung forken.

## Schritt 1

- Account bei Gitlab.com erstellen
- Praktikum-IV-Vorlage importieren
- Neues Projekt erstellen


## Schritt 2

- Runner für Projekt registrieren
- shared runner deaktivieren


    - $ sudo gitlab-runner register
    - $ sudo gitlab-runner start
    - $ docker run -d -p 80:80 --name my-nginx nginx


## Schritt 3

- in .gitlab-ci.yml die Stage "lint" erweitern
- npm als Paketmanager
- mit Prettier das Format ändern
- Pipeline testen


## Schritt 4

- folgende Punkte hunzufügen:
    - Check, ob Änderungen vorgenommen wurden
    - einen Commit mit geeigneter Commit-Nachricht erstellen (ggf. mit "-a" arbeiten)
    - Ggf. Git mit meinen Benutzerinfos konfigurieren
    - Änderungen auf den aktuellen Branch pushen

- zusätzl. Schritte
    - Access-Token erstellen
    - Settings -> CI/CD -> Variables (Variable hinzufügen)
    - Push-Befehl anpassen


## Schritt 5

- Stage "test" erweitern, um Test auszuführen
- npm nutzen, zum Test auszuführen
- prüfen, ob "npm clean-install" angebracht ist
- Code-Coverage - Bericht in das GitLab-Projekt einbinden
- Coverage - Report und Pipeline Status in README.md einbinden


## Schritt 6

- Code in "sum.routers.ts" entsprechend den Voraussetzungen vervollständigen
- mithilfe der Pipeline den Fortschritt des Codes überprüfen

